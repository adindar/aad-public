% Hysteretic curve'in çizdirilmesi
% SON GÜNCELLEME 100507
clc, close all;
minx=min(data(:,1)); maxx=max(data(:,1)); miny=min(data(:,2)); maxy=max(data(:,2));
	ratiox=maxx-minx; ratioy=maxy-miny;
%     aviobj = avifile(filename(:,1:end-4),'fps',12); % here the grapname comes from the discretization file.
    aviobj = avifile([pathname dirname,'\',filename(1:end-4)],'fps',12); % here the grapname comes from the discretization file.

figure(1)
for i=1:5:size(data,1)-1
    plot(data(1:i,1),(data(1:i,2)),'b-',data(i,1),(data(i,2)),'r*'),grid
		line([minx-ratiox/10,maxx+ratiox/10],[0,0],'Color','k','LineWidth',2),
		line([0,0],[miny-ratioy/10,maxy+ratioy/10],'Color','k','LineWidth',2),
        axis([minx-ratiox/10 maxx+ratiox/10 miny-ratioy/10 maxy+ratioy/10]),
        title([filename(:,1:end-4),' ',sprintf('Data number : %0.5g ',i),'/',num2str(length(data(:,1)))]);
        xlabel('Displacement'),ylabel('Force')
	text(minx,miny*1.1,...
	[sprintf('force= %0.2g, Disp= %0.2g ',data(i,2),data(i,1))],'BackgroundColor',[1,1,1])

   t=getframe(gcf);
       aviobj = addframe(aviobj,t);
end
aviobj = close(aviobj); % 
