% LOOPLARIN AYRIKLA�TIRILMASI PROGRAMI
% SON G�NCELLEME 0607107
% Ahmet An�l Dindar (adindar@iku.edu.tr)
tic
 clc, clear, close all;
%% hay�rl� olsun directory'den sonra s�ra geri kalan i�lemlerde
[filename, pathname]=uigetfile('*.txt','Hey Friend! select your hysteresis data!');
data=load ([pathname filename]);
dirname=filename(:,1:end-4);
mkdir([pathname dirname,'\',dirname,'loop graphs']);
mkdir([pathname dirname,'\',dirname,'loop text ']);
format('short');
%% elimizdeki hysteretic egrinin bir g�r�n�m�ne bakal�m :
	scrsz = get(0,'ScreenSize');
	figure('Position',[1 scrsz(4)/3 scrsz(3)/2 scrsz(4)/2])
	minx=min(data(:,1)); maxx=max(data(:,1)); miny=min(data(:,2)); maxy=max(data(:,2));
	ratiox=maxx-minx; ratioy=maxy-miny;
		plot(data(:,1),data(:,2),'r-','linewidth',2), grid
		axis([minx-ratiox/10 maxx+ratiox/10 miny-ratioy/10 maxy+ratioy/10]),
        line([minx-ratiox/10,maxx+ratiox/10],[0,0],'Color','k','LineWidth',2),
		line([0,0],[miny-ratioy/10,maxy+ratioy/10],'Color','k','LineWidth',2),
        title(filename);
        xlabel('Displacement (m)'), ylabel('Force (kN)')
   print('-dpng',[pathname dirname,'\',dirname,'loop graphs','\',dirname,'-Hysteresis Loop']);

%% simdi is ayr�kla�t�rmada - YAN� ���N KOPTU�U YER ASLINDA BURASI
		x=data(:,1); y=data(:,2);
		t=1; % loop ad�n� koymak i�in gerekli...
		n=2; % her bir loopta ikinci eleman data'dan al�n�yor. zira ilk eleman, zerohold diye ayr�ca hesaplan�yor.
		zerohold=0;    
		totalalan=0;
for i=2:size(data,1)-1;
    loop(1,:)=zerohold;
 % force i�in ayn� i�aretli olan b�lgede loop yarat�l�yor
    if sign(y(i))==sign(y(i+1))
        loop(n,1:2)=data(i,:);
        n=n+1; % burada n art�k her bir loop'taki eleman yerle�tirmede kullan�lan indekstir.
        else
            loop(n,1:2)=data(i,:); % BOYLECE ��ARET DE��S�M�NDEN ONCEK� SON ELEMAN DA ALINDI
            % bu if-end aras� loop'un kapand��� yeri belirlemek i�indir  
                if sign(y(i+1))==0 % e�er loopun son de�eri 0'a e�itse...
					zerohold=data(i+1,:);
                    loop(n+1,:)=zerohold;
                    y(i+1)=-1*sign(y(i))*.0000000001;  % burada bir trick var. s�f�ra yakla�ma problem oldu�undan burada s�f�ra �ok yak�n bir rakam olu�turuluyor.
                    c=i;   
                elseif abs(x(i))>abs(x(i+1))
						zerohold=[x(i)-sign(x(i))*((abs(x(i))-abs(x(i+1)))*(abs(y(i)))/(abs(y(i+1))+abs(y(i)))), 0];
                        loop(n+1,:)=zerohold;
                        c=i;
                        else 
							zerohold=[x(i)-sign(x(i))*((abs(x(i))-abs(x(i+1)))*(abs(y(i)))/(abs(y(i+1))+abs(y(i)))), 0];
							loop(n+1,:)=zerohold;
                        c=i;    
                end
                 halfloop{t,1}=loop;  % array'in olu�turulmas�
%                  save([dirname,'\loop text\',filename(:,1:end-4),'-halfloop # ',num2str(t),'.txt'],'loop','-ascii');
if t<10
                 dlmwrite([pathname dirname,'\',dirname,'loop text\',filename(:,1:end-4),'-halfloop # 00',num2str(t),'.txt'],loop,'delimiter','\t','precision','%3.2f')
elseif t<100
              dlmwrite([pathname dirname,'\',dirname,'loop text\',filename(:,1:end-4),'-halfloop # 0',num2str(t),'.txt'],loop,'delimiter','\t','precision','%3.2f')
else 
              dlmwrite([pathname dirname,'\',dirname,'loop text\',filename(:,1:end-4),'-halfloop # ',num2str(t),'.txt'],loop,'delimiter','\t','precision','%3.2f')
end
                 t=t+1; n=2; clear loop;
        end
end
%% bakal�m looplar nas�l olu�uyor
s=1;cumalan=0;
for i=1:2:size(halfloop,1)-1
                    fulloop=[(halfloop{i,1});(halfloop{i+1,1})];  
                    scrsz = get(0,'ScreenSize');
					figure('Position',[650 scrsz(4)/3 scrsz(3)/2 scrsz(4)/2])
                     axis([minx-ratiox/10 maxx+ratiox/10 miny-ratioy/10 maxy+ratioy/10]),
		            plot(fulloop(:,1),fulloop(:,2),'b-','LineWidth',2), ,grid;
						line([minx-ratiox/10,maxx+ratiox/10],[0,0],'Color','k','LineWidth',2),
						line([0,0],[miny-ratioy/10,maxy+ratioy/10],'Color','k','LineWidth',2),
                    alan=polyarea(fulloop(:,1),fulloop(:,2));
                    cumalan=cumalan+alan;
                    totalalan(s,1:3)=[s alan cumalan];
                    title(sprintf('%0.5g. loop alan degeri %0.5g force*length',s,alan));
                    xlabel('Displacement (m)'), ylabel('Force (kN)')
                    if s<10
                         graphname = ([filename(1:end-4),' loop # 0',num2str(s)]);
                    else
                        graphname = ([filename(1:end-4),' loop # ',num2str(s)]);
                    end
            print('-dpng',[pathname dirname,'\',dirname,'loop graphs','\',graphname]);
            s=s+1;
            close(gcf)
end
sprintf('Toplam Alan degeri %0.5g force*length \n',sum(totalalan(:,2)))
format short g
%% Bakal�m enerji da��l�m� nas�l oluyor
figure(s)
subplot(2,1,1),bar(totalalan(:,1),totalalan(:,2)),grid
    title([dirname,'-Energy Graph',sprintf('- (%0.5g force*length) \n',sum(totalalan(:,2)))])
    ylabel('Energy per loop')
    legend('Hysteresis Energy',2)
subplot(2,1,2),bar(totalalan(:,1),totalalan(:,3)),grid
    xlabel('Loop Numbers'),ylabel('Cumulative Energy per loop')
    legend('Cumulative Hysteresis Energy',2)
print('-dpng',[pathname dirname,'\Energy Graphs (Loops']);
dlmwrite([pathname dirname,'\',dirname,'-energy.txt'],totalalan,'delimiter','\t','precision','%3.2f')
%% -------------- GAME OVER --------------------------------
close all
toc
