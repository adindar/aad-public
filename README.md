# Merhaba

Bu repository'nin amacı Ahmet Anıl Dindar'ın yazmış olduğu bilimsel ve mühendislik hesaplamalarına ait bilgisayar kodlarının paylaşılmasıdır. 

![2014-1027-aad-public-small.png](https://bitbucket.org/repo/5nnkx8/images/248560014-2014-1027-aad-public-small.png)

## Bilgi güçtür, paylaşmak kolaydır :)

Bilgi edinilmesi zorlayıcı olabilen ama sonuçları elde edildiğinde paylaşılması kolay olan bir güçtür. Kişisel olarak edindiğim misyon "üretmek ve daha çok üretmek" olduğundan bu repository ile paylaşıma katkıda bulunacağımı düşünüyorum. 

Ahmet Anıl Dindar (2014_1027 / Istanbul)

İletişim için 
Dr. Ahmet Anıl Dindar

[adindar@iku.edu..tr](adindar@iku.edu.tr)

[https://twitter.com/ahmetanildindar](https://twitter.com/ahmetanildindar)

[ahmetanildindar.blogspot.com](ahmetanildindar.blogspot.com)